package cn.tzp.user.ssm.domain;

public class Menu {
    private Short menuId;

    private String name;

    private Short fatherId;

    private String link;

    private String iconfont;

    public Short getMenuId() {
        return menuId;
    }

    public void setMenuId(Short menuId) {
        this.menuId = menuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Short getFatherId() {
        return fatherId;
    }

    public void setFatherId(Short fatherId) {
        this.fatherId = fatherId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link == null ? null : link.trim();
    }

    public String getIconfont() {
        return iconfont;
    }

    public void setIconfont(String iconfont) {
        this.iconfont = iconfont == null ? null : iconfont.trim();
    }
}